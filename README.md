# BFJ.java

RnDnet job helper scripts for Java.

Requirements: Java 8

Adapter for reading and writing RnDnet packages, allows to read parameters from .prm file.
See EmptyFilterSample as example of use. It works also with packages downloaded to the local folder.

Build from sources by maven:
```
mvn clean install -Dmaven.test.skip=true
mvn assembly:assembly
```

You can also download bin/bfj.jar and it's dependences 
(find "Files" and click "jar" link):

commons-io-2.8.0.jar ( https://mvnrepository.com/artifact/commons-io/commons-io/2.8.0 )  
gson-2.8.6.jar ( https://mvnrepository.com/artifact/com.google.code.gson/gson/2.8.6 )  
jhdf5-14.12.0.jar ( https://mvnrepository.com/artifact/ch.systems.cisd/jhdf5/14.12.0 )
