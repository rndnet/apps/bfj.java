package com.rndnet.bfj;

import java.io.File;
import java.io.IOException;

public class RndnetAdapter {
    public static final String META_ZIP_NAME = ".data.zip";

    private final boolean readOnly;
    private final File pkgPath;
    private final File outPath;
    private final File parPath;
    private final String srcName;
    private final ParameterFactory params;
    private RndnetPackageIn pkgIn;
    private RndnetPackageOut pkgOut;

    /**
     * Read only adapter
     * @param pkgPath - path to input package
     * @throws Exception -
     */
    public RndnetAdapter(String pkgPath) throws Exception {
        this(pkgPath, null, null, true);
    }

    /**
     * Read/write adapter
     * @param pkgPath - path to input package
     * @param outPath - path to output package
     * @param parPath - path to parameter file
     * @throws Exception -
     */
    public RndnetAdapter(String pkgPath, String outPath, String parPath) throws Exception {
        this(pkgPath, outPath, parPath, false);
    }

    private RndnetAdapter(String pkgPath, String outPath, String parPath, boolean readOnly) throws Exception {
        this.readOnly = readOnly;
        checkArguments(readOnly, pkgPath, outPath, parPath);
        this.pkgPath = new File(pkgPath);
        srcName = new File(pkgPath).getName();
        pkgIn = new RndnetPackageIn(this.pkgPath);
        if (!readOnly) {
            this.outPath = new File(outPath);
            this.parPath = new File(parPath);
            params = new ParameterFactory(parPath);
            pkgOut = new RndnetPackageOut(this.outPath);
        } else {
            this.outPath = null;
            this.parPath = null;
            params = null;
            pkgOut = null;
        }
    }

    public File getPkgPath() {
        return pkgPath;
    }

    public File getOutPath() {
        return outPath;
    }

    public File getParPath() {
        return parPath;
    }

    public String getSrcName() {
        return srcName;
    }

    public ParameterFactory getParams() {
        return params;
    }

    public RndnetPackageIn getPkgIn() {
        return pkgIn;
    }

    public RndnetPackageOut getPkgOut() {
        return pkgOut;
    }

    /**
     * Check required parameters
     * @param parNames -
     * @throws Exception -
     */
    public void checkParameters(String[] parNames) throws Exception {
        final String msg = "Parameter %s expected";
        final StringBuilder sb = new StringBuilder();
        for (String parName : parNames) {
            if (!params.hasPar(parName)) {
                sb.append('\n').append(String.format(msg, parName));
            }
        }
        if (sb.length() > 0) {
            throw new IllegalArgumentException(sb.toString());
        }
    }

    /**
     * Check arguments
     * @param args - pkgPath, outPath, parPath
     * @throws Exception -
     */
    public static void checkArguments(String... args) throws Exception {
        checkArguments(false, args);
    }

    public static void checkArguments(boolean readOnly, String... args) throws Exception {
        int cnt = readOnly ? 1 : 2;
        if (args.length < cnt) {
            throw new IllegalArgumentException("Expected parameters: " + cnt + ", found: " + args.length);
        }
        final File pkgPath = new File(args[0]);
        if ((!pkgPath.exists() || !pkgPath.isDirectory()) && !new File(pkgPath.getParentFile(), META_ZIP_NAME).exists()) {
            throw new IllegalArgumentException("Signal path does not exist: " + pkgPath);
        }
        if (readOnly) return;
        final File outPath = new File(args[1]);
        if (!outPath.exists() || !outPath.isDirectory()) {
            throw new IllegalArgumentException("Out path does not exist: " + outPath);
        }
        if (args.length > 2) {
            final File parPath = new File(args[2]);
            if (!parPath.exists() || !parPath.isFile()) {
                throw new IllegalArgumentException("Parameter file does not exist: " + parPath);
            }
        }
    }

    /**
     * Create new out package in folder with name = lastname+1, save label in it
     * @param label - label
     * @return - out package
     * @throws IOException -
     */
    public RndnetPackageOut createPackage(String label) throws IOException {
        if (readOnly) {
            return null;
        }
        final File outRoot = outPath.getParentFile();
        final File[] dirs = outRoot.listFiles(File::isDirectory);
        int n = 0;
        if (dirs != null) {
            for (File dir : dirs) {
                try {
                    int dn = Integer.parseInt(dir.getName());
                    if (n < dn) {
                        n = dn;
                    }
                } catch (NumberFormatException e) {
                    // do nothing
                }
            }
        }
        final File newPkg = new File(outRoot, String.valueOf(n + 1));
        newPkg.mkdir();
        final RndnetPackageOut pkg = new RndnetPackageOut(newPkg);
        pkg.setLabel(label);
        pkg.save();
        return pkg;
    }

}
