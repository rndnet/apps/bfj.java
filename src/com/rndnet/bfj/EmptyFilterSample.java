package com.rndnet.bfj;

import org.apache.commons.lang.time.DateUtils;

/**
 * Just a sample
 * Read RnDnet package with signal, do something useless, write package as is.
 */
public class EmptyFilterSample {
    private final String P_TIME_FROM = "signal_time_from";
    private final String P_TIME_TO = "signal_time_to";
    private final String P_DATA = "signal_data";

    final RndnetAdapter adapter;

    /**
     * Read/write adapter
     * @param pkgPath - path to input package
     * @param outPath - path to output package
     * @param parPath - path to parameter file
     * @throws Exception -
     */
    public EmptyFilterSample(String pkgPath, String outPath, String parPath) throws Exception {
        adapter = new RndnetAdapter(pkgPath, outPath, parPath);
    }

    public void run() throws Exception {
        // Check parameters
        String[] parNames = new String[]{"size", "overlap"};
        try {
            adapter.checkParameters(parNames);
        } catch (Exception e) {
            // do something or exit
            e.printStackTrace();
        }

        final RndnetPackageIn pkgIn = adapter.getPkgIn();
        final RndnetPackageOut pkgOut = adapter.getPkgOut();

        // Load package h5 data if it needs
        pkgIn.loadH5();
        // Do something
        final RndnetVar varData = pkgIn.getVar(P_DATA);
        final double[][] data = varData.getTransposedDoubleVal();
        final int chCnt = data.length;
        final double step = ((double) (pkgIn.getVar(P_TIME_TO).getDateVal().getTime() - pkgIn.getVar(P_TIME_FROM).getDateVal().getTime()) / DateUtils.MILLIS_PER_SECOND) / (data[0].length - 1);
        double[][] res = new double[chCnt][];
        for (int ch = 0; ch < chCnt; ch++) {
            System.out.println("Channel: " + ch);
            res[ch] = data[ch];
        }

        // Save res only - sample
//        final RndnetVar dataVar = pkgOut.addOutVar(P_DATA, true);
//        dataVar.saveTransposedDoubleVal(res);

        // Save meta & h5 variables of 'in' package to 'out'
        //  Don't save other files, copy them manually!
        pkgOut.copyOf(pkgIn);
        // pkgOut.setLabel("newLabel");
        // Remove some variables
        pkgOut.removeVar(P_DATA);
        pkgOut.save();
        // Add res variable and save it by special way
        final RndnetVar resVar = pkgOut.addOutVar("res", true);
        resVar.saveTransposedDoubleVal(res);

        // Create more then one out packages
        final RndnetPackageOut pkgOut2 = adapter.createPackage("my new package");  // label
        pkgOut2.addOutVar("myVar", false).saveVal(365);
        pkgOut2.addOutVar("myStr", false).saveVal("Some string");
    }

    public static void main(String[] args) throws Exception {
        if (args.length < 3) {
            System.out.println("Wrong initial parameters, expected: pkgPath outPath parPath");
            System.exit(1);
        }
        new EmptyFilterSample(args[0], args[1], args[2]).run();
    }

}
