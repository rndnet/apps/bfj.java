package com.rndnet.bfj;

import java.io.File;
import java.io.IOException;

public class RndnetPackageOut extends RndnetPackage {

    /**
     * RndnetPackageOut - output package
     * @param path - path to output package folder
     */
    public RndnetPackageOut(File path) {
        super(path);
    }

    @Override
    protected void init() {
        if (!metaPath.exists()) {
            metaPath.mkdir();
        }
        try {
            label = Utils.loadFile(labelPath);
        } catch (IOException e) {
            label = "";
        }
    }

    public RndnetVar addOutVar(String name, boolean h5Flag) {
        final RndnetVar var = new RndnetVar(this, name, h5Flag);
        vars.put(name, var);
        return var;
    }

    public RndnetVar addOutVar(String name, boolean h5Flag, RndnetVar.Type type, boolean doublePrec) {
        final RndnetVar var = new RndnetVar(this, name, h5Flag, type, doublePrec);
        vars.put(name, var);
        return var;
    }

    /**
     * Save meta & h5 variables
     * Don't save other files, copy them manually!
     * @throws IOException -
     */
    public void save() throws IOException {
        Utils.saveToFile(labelPath, label);
        for (String key : vars.keySet()) {
            try {
                getVar(key).save();
            } catch (Exception e) {
                System.out.println("Can't save variable: " + key);
                throw e;
            }
        }
    }

}
