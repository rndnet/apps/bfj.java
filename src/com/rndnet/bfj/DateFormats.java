package com.rndnet.bfj;

import sun.util.calendar.ZoneInfo;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

public class DateFormats {
    public static final TimeZone ZERO_TIME_ZONE = new ZoneInfo("GMT+0", 0);
    public static final String PKG_DATE_FORMAT = "yyyy.MM.dd HH:mm:ss.SSS";

    public static Date parseDate(final String format, final String strDate, final TimeZone timeZone) throws ParseException {
        final SimpleDateFormat sdf = new SimpleDateFormat(format);
        if (timeZone != null) {
            sdf.setCalendar(Calendar.getInstance(timeZone));
        }
        return sdf.parse(strDate);
    }

    public static Date parseDateZero(final String format, final String strDate) throws ParseException {
        return parseDate(format, strDate, ZERO_TIME_ZONE);
    }

    public static String formatDate(final String format, final Date date, final TimeZone timeZone) {
        final SimpleDateFormat sdf = new SimpleDateFormat(format);
        if (timeZone != null) {
            sdf.setCalendar(Calendar.getInstance(timeZone));
        }
        return sdf.format(date);
    }

    public static String formatDateZero(final String format, final Date date) {
        return formatDate(format, date, ZERO_TIME_ZONE);
    }

}
