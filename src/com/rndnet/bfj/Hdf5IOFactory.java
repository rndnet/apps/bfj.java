package com.rndnet.bfj;

import ch.systemsx.cisd.hdf5.*;

import java.io.File;
import java.util.Date;

public class Hdf5IOFactory {
    // Write
    public static IHDF5Writer createH5Writer(String filePath) {
        return createH5Writer(new File(filePath));
    }

    public static IHDF5Writer createH5Writer(File file) {
        final IHDF5WriterConfigurator configurator = HDF5FactoryProvider.get().configure(file);
        configurator.useUTF8CharacterEncoding();
        return configurator.writer();
    }

    public static void closeH5Writer(IHDF5Writer out) {
        if (out != null) {
            out.close();
        }
    }

    public static void writeH5Value(IHDF5Writer out, String name, Object val) {
        if (val instanceof String) {
            writeH5String(out, name, (String) val);
        } else if (val instanceof String[]) {
            writeH5StringArray(out, name, (String[]) val);
        } else if (val instanceof Integer) {
            writeH5Int(out, name, (Integer) val);
        } else if (val instanceof int[]) {
            writeH5IntArray(out, name, (int[]) val);
        } else if (val instanceof int[][]) {
            writeH5IntMatrix(out, name, (int[][]) val);
        } else if (val instanceof Double) {
            writeH5Double(out, name, (Double) val);
        } else if (val instanceof double[]) {
            writeH5DoubleArray(out, name, (double[]) val);
        } else if (val instanceof double[][]) {
            writeH5DoubleMatrix(out, name, (double[][]) val);
        } else if (val instanceof Float) {
            writeH5Float(out, name, (Float) val);
        } else if (val instanceof float[]) {
            writeH5FloatArray(out, name, (float[]) val);
        } else if (val instanceof float[][]) {
            writeH5FloatMatrix(out, name, (float[][]) val);
        } else if (val instanceof Date) {
            writeH5DateAsString(out, name, (Date) val);
        } else if (val instanceof Date[]) {
            writeH5DateArrayAsStrings(out, name, (Date[]) val);
        } else {
            throw new RuntimeException("Unsupported type: " + val.getClass());
        }
    }

    public static void writeH5String(IHDF5Writer out, String name, String val) {
        if (val != null) {
            out.string().writeArrayVL(name, new String[]{val});
        }
    }

    public static void writeH5StringArray(IHDF5Writer out, String name, String[] val) {
        if (val != null && val.length > 0) {
            out.string().writeArrayVL(name, val);
        }
    }

    public static void writeH5Int(IHDF5Writer out, String name, Integer val) {
        if (val != null) {
            out.writeInt(name, val);
        }
    }

    public static void writeH5IntArray(IHDF5Writer out, String name, int[] val) {
        if (val != null && val.length > 0) {
            out.int32().writeArray(name, val, HDF5IntStorageFeatures.INT_DEFLATE);
        }
    }

    public static void writeH5IntMatrix(IHDF5Writer out, String name, int[][] val) {
        if (val != null && val.length > 0) {
            out.int32().writeMatrix(name, val, HDF5IntStorageFeatures.INT_DEFLATE);
        }
    }

    public static void writeH5Double(IHDF5Writer out, String name, Double val) {
        if (val != null) {
            out.writeDouble(name, val);
        }
    }

    public static void writeH5DoubleArray(IHDF5Writer out, String name, double[] val) {
        if (val != null && val.length > 0) {
            out.float64().writeArray(name, val, HDF5FloatStorageFeatures.FLOAT_DEFLATE);
        }
    }

    public static void writeH5DoubleMatrix(IHDF5Writer out, String name, double[][] val) {
        if (val != null && val.length > 0) {
            out.float64().writeMatrix(name, val, HDF5FloatStorageFeatures.FLOAT_DEFLATE);
        }
    }

    public static void writeH5Float(IHDF5Writer out, String name, Float val) {
        if (val != null) {
            out.writeFloat(name, val);
        }
    }

    public static void writeH5FloatArray(IHDF5Writer out, String name, float[] val) {
        if (val != null && val.length > 0) {
            out.float32().writeArray(name, val, HDF5FloatStorageFeatures.FLOAT_DEFLATE);
        }
    }

    public static void writeH5FloatMatrix(IHDF5Writer out, String name, float[][] val) {
        if (val != null && val.length > 0) {
            out.float32().writeMatrix(name, val, HDF5FloatStorageFeatures.FLOAT_DEFLATE);
        }
    }

    public static void writeH5Boolean(IHDF5Writer out, String name, Boolean val) {
        if (val != null) {
            out.writeBoolean(name, val);
        }
    }

    public static void writeH5Date(IHDF5Writer out, String name, Date val) {
        if (val != null) {
            out.writeDate(name, val);
        }
    }

    public static void writeH5DateArray(IHDF5Writer out, String name, Date[] val) {
        if (val != null && val.length > 0) {
            out.writeDateArray(name, val);
        }
    }

    public static void writeH5DateAsString(IHDF5Writer out, String name, Date val) {
        if (val != null) {
            writeH5String(out, name, DateFormats.formatDateZero(DateFormats.PKG_DATE_FORMAT, val));
        }
    }

    public static void writeH5DateArrayAsStrings(IHDF5Writer out, String name, Date[] val) {
        if (val != null && val.length > 0) {
            final String[] strs = new String[val.length];
            for (int i = 0; i < strs.length; i++) {
                strs[i] = DateFormats.formatDateZero(DateFormats.PKG_DATE_FORMAT, val[i]);
            }
            out.writeStringArray(name, strs);
        }
    }

    // Read
    public static IHDF5Reader createH5Reader(String filePath) {
        return createH5Reader(new File(filePath));
    }

    public static IHDF5Reader createH5Reader(File file) {
        return HDF5FactoryProvider.get().openForReading(file);
    }

    public static void closeH5Reader(IHDF5Reader in) {
        in.close();
    }

    public static final String H5DT_BITFIELD = "BITFIELD";
    public static final String H5DT_ENUM = "ENUM";
    public static final String H5DT_INTEGER = "INTEGER";
    public static final String H5DT_FLOAT = "FLOAT";
    public static final String H5DT_STRING = "STRING";
    public static final String H5DT_OPAQUE = "OPAQUE";
    public static final String H5DT_BOOLEAN = "BOOLEAN";
    public static final String H5DT_COMPOUND = "COMPOUND";
    public static final String H5DT_REFERENCE = "REFERENCE";
    public static final String H5DT_OTHER = "OTHER";

    public static Object readH5Value(IHDF5Reader in, String name) {
        final HDF5ObjectType type = in.object().getObjectType(name);
        if (!"DATASET".equals(type.name())) {
            return null;
        }
        final HDF5DataSetInformation info = in.getDataSetInformation(name);
        final String dataType = info.getTypeInformation().getDataClass().name();
        final boolean isScalar = info.isScalar();
        final boolean isMatrix = !isScalar && info.getDimensions().length == 2;

        if (H5DT_STRING.equals(dataType)) {
            if (isScalar) {
                return readH5String(in, name);
            } else {
                return readH5StringArray(in, name);
            }
        } else if (H5DT_INTEGER.equals(dataType)) {
            if (isScalar) {
                return readH5Int(in, name, 0);
            } else if (isMatrix) {
                return readH5IntMatrix(in, name);
            } else {
                return readH5IntArray(in, name);
            }
        } else if (H5DT_FLOAT.equals(dataType)) {
            final int size = info.getTypeInformation().getElementSize();
            if (isScalar) {
                return size == 4 ? readH5Float(in, name) : readH5Double(in, name);
            } else if (isMatrix) {
                return size == 4 ? readH5FloatMatrix(in, name) : readH5DoubleMatrix(in, name);
            } else {
                return size == 4 ? readH5FloatArray(in, name) : readH5DoubleArray(in, name);
            }
        } else if (H5DT_BOOLEAN.equals(dataType)) {
            return isScalar ? in.readBoolean(name) : null;
        } else if (info.isTimeStamp()) {
            if (isScalar) {
                return readH5Date(in, name);
            } else {
                return readH5DateArray(in, name);
            }
        } else {
            System.out.println("Hdf5IOFactory.readH5Value(). Not implemented yet for data type " + dataType);
            return null;
        }
    }

    public static String readH5String(IHDF5Reader in, String name) {
        try {
            return in.string().read(name);
        } catch (Exception e) {
            try {
                final int[] chs = in.readIntArray(name);
                final StringBuilder sb = new StringBuilder();
                for (int ch = 0; ch < chs.length; ch++) {
                    sb.append((char) chs[ch]);
                }
                return sb.toString();
            } catch (Exception e1) {
                return null;
            }
        }
    }

    public static String[] readH5StringArray(IHDF5Reader in, String name) {
        try {
            return in.string().readArray(name);
        } catch (Exception e) {
            return null;
        }
    }

    public static int readH5Int(IHDF5Reader in, String name, int defaultVal) {
        try {
            return in.readInt(name);
        } catch (Exception e) {
            return defaultVal;
        }
    }

    public static int[] readH5IntArray(IHDF5Reader in, String name) {
        try {
            return in.readIntArray(name);
        } catch (Exception e) {
            return null;
        }
    }

    public static int[][] readH5IntMatrix(IHDF5Reader in, String name) {
        try {
            return in.readIntMatrix(name);
        } catch (Exception e) {
            return null;
        }
    }

    public static double readH5Double(IHDF5Reader in, String name) {
        try {
            return in.readDouble(name);
        } catch (Exception e) {
            return Double.NaN;
        }
    }

    public static double[] readH5DoubleArray(IHDF5Reader in, String name) {
        try {
            return in.readDoubleArray(name);
        } catch (Exception e) {
            return null;
        }
    }

    public static double[][] readH5DoubleMatrix(IHDF5Reader in, String name) {
        try {
            return in.readDoubleMatrix(name);
        } catch (Exception e) {
            return null;
        }
    }

    public static float readH5Float(IHDF5Reader in, String name) {
        try {
            return in.readFloat(name);
        } catch (Exception e) {
            return Float.NaN;
        }
    }

    public static float[] readH5FloatArray(IHDF5Reader in, String name) {
        try {
            return in.readFloatArray(name);
        } catch (Exception e) {
            return null;
        }
    }

    public static float[][] readH5FloatMatrix(IHDF5Reader in, String name) {
        try {
            return in.readFloatMatrix(name);
        } catch (Exception e) {
            return null;
        }
    }

    public static Date readH5Date(IHDF5Reader in, String name) {
        try {
            return in.readDate(name);
        } catch (Exception e) {
            return null;
        }
    }

    public static Date[] readH5DateArray(IHDF5Reader in, String name) {
        try {
            return in.readDateArray(name);
        } catch (Exception e) {
            return null;
        }
    }

    public static Date readH5DateAsString(IHDF5Reader in, String name) {
        try {
            return DateFormats.parseDateZero(DateFormats.PKG_DATE_FORMAT, in.readString(name));
        } catch (Exception e) {
            try {
                final int[] chs = in.readIntArray(name);
                final StringBuilder sb = new StringBuilder();
                for (int ch : chs) {
                    sb.append((char) ch);
                }
                return DateFormats.parseDateZero(DateFormats.PKG_DATE_FORMAT, sb.toString());
            } catch (Exception e1) {
                return null;
            }
        }
    }

    public static Date[] readH5DateArrayAsString(IHDF5Reader in, String name) {
        try {
            final String[] strs = in.readStringArray(name);
            final Date[] dates = new Date[strs.length];
            for (int i = 0; i < dates.length; i++) {
                dates[i] = DateFormats.parseDateZero(DateFormats.PKG_DATE_FORMAT, strs[i]);
            }
            return dates;
        } catch (Exception e) {
            return null;
        }
    }

    // Convert
    public static float[][] matrixDoubleToFloat(double[][] data) {
        final float[][] fdata = new float[data.length][];
        for (int i = 0; i < data.length; i++) {
            fdata[i] = new float[data[i].length];
            for (int j = 0; j < data[i].length; j++) {
                fdata[i][j] = (float)data[i][j];
            }
        }
        return fdata;
    }

}
