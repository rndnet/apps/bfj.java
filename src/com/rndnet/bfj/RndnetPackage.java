package com.rndnet.bfj;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.HashMap;
import java.util.Map;

public abstract class RndnetPackage {
    protected Map<String, RndnetVar> vars = new HashMap();
    protected File path;
    protected File labelPath;
    protected File metaPath;
    protected File metaZip;
    protected String label = "";

    /**
     * RndnetPackage
     * @param path - path to package folder
     */
    public RndnetPackage(File path) {
        this.path = path;
        labelPath = new File(path, "label");
        metaPath = new File(path, "meta");
        final File zip = new File(path.getParentFile(), RndnetAdapter.META_ZIP_NAME);
        metaZip = zip.exists() ? zip : null;
        init();
    }

    protected abstract void init();

    public File getPath() {
        return path;
    }

    public File getMetaPath() {
        return metaPath;
    }

    public File getLabelPath() {
        return labelPath;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public void setVarType(String name, RndnetVar.Type type) {
        vars.get(name).setType(type);
    }

    public boolean hasVar(String name) {
        return vars.containsKey(name);
    }

    public RndnetVar getVar(String name) {
        if (vars.containsKey(name)) {
            return vars.get(name);
        } else {
            throw new IllegalArgumentException("Variable not found: " + name);
        }
    }

    public Map<String, RndnetVar> getVars() {
        return vars;
    }

    public void addVar(String name, RndnetVar var) {
        var.setPkg(this);
        vars.put(name, var);
    }

    public void removeVar(String name) {
        vars.remove(name);
    }

    /**
     * Method don't change package label
     */
    public void copyOf(RndnetPackage srcPkg) {
        for (String key : srcPkg.getVars().keySet()) {
            final RndnetVar var = srcPkg.getVar(key);
            addVar(key, var.clone());
        }
    }

    /**
     * Load meta variables from .data.zip or from meta folder
     * Do not load values from h5
     */
    protected void loadMeta() {
        if (metaZip != null) {            // load meta from zip
            try {
                FileSystem zipFs = FileSystems.newFileSystem(metaZip.toPath(), null);
                Files.walkFileTree(zipFs.getPath("/"), new SimpleFileVisitor<Path>() {
                    @Override
                    public FileVisitResult visitFile(Path path, BasicFileAttributes attrs) throws IOException {
//                        System.out.println(path);
                        String val = "";
                        try (InputStream is = Files.newInputStream(path)) {
                            val = IOUtils.toString(is, StandardCharsets.UTF_8);
                        }
                        final String name = path.getName(path.getNameCount() - 1).toString();
                        final String parent = path.getName(path.getNameCount() - 2).toString();
                        if ("label".equals(name)) {
                            label = val;
                        } else if ("meta".equals(parent)) {
                            final RndnetVar var = new RndnetVar(RndnetPackage.this, name, name);
                            var.parseMeta(val);
                            vars.put(name, var);
                        }
                        return FileVisitResult.CONTINUE;
                    }
                });

            } catch (IOException e) {
                System.out.println("Wrong meta zip file: " + metaZip.getAbsolutePath());
                e.printStackTrace();
            }
        } else {            // load meta from <pkg>/meta folder
            // load label
            try {
                label = Utils.loadFile(labelPath);
            } catch (IOException e) {
//                System.out.println("Wrong package label: " + e.getMessage());
            }
            if (metaPath.exists()) {    // load meta
                final File[] meta = new File(path, "meta").listFiles(File::isFile);
                if (meta != null) {
                    for (File f : meta) {
                        final String name = FilenameUtils.getBaseName(f.getName());
                        final RndnetVar var = new RndnetVar(this, f.getName(), name);
                        vars.put(name, var);
                        try {
                            var.load();
                        } catch (IOException e) {
                            System.out.println("Wrong meta variable: " + var.getName());
                            e.printStackTrace();
                        }
                    }
                }
            }
        }
    }

    /**
     * Load values from h5
     */
    public void loadH5() {
        for (RndnetVar var : vars.values()) {
            if (var.isH5Flag()) {
                try {
                    var.load();
                } catch (IOException e) {
                    System.out.println("Wrong H5 variable: " + var.getName());
                    e.printStackTrace();
                }
            }
        }
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        for (RndnetVar var : vars.values()) {
            sb.append("\n  ").append(var.toString());
        }
        return this.getClass().getName() + "{" +
                "path=" + path.getAbsolutePath() +
                sb.toString() +
                '}';
    }

}
