package com.rndnet.bfj;

import org.apache.commons.io.FilenameUtils;

import java.io.File;

public class RndnetPackageIn extends RndnetPackage {

    /**
     * RndnetPackageIn - input package
     * @param path - path to input package folder
     */
    public RndnetPackageIn(File path) {
        super(path);
    }

    @Override
    protected void init() {
        loadMeta();
        // only list, load by request
        final File[] files = path.listFiles(File::isFile);
        if (files != null) {
            for (File f : files) {
                final String name = FilenameUtils.getBaseName(f.getName());
                vars.put(name, new RndnetVar(this, f.getName(), name));
            }
        }
    }

    public static void main(String[] args) {
        final RndnetPackage rp = new RndnetPackageIn(new File(args[0]));
        System.out.println(rp.toString());
    }

}
