package com.rndnet.bfj;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import static com.rndnet.bfj.Utils.*;

public class ParameterFactory {
    protected Map<String, String> pars;

    public ParameterFactory(String file) {
        ParameterParser parser = isRndnet(new File(file).getName()) ? new RndnetParameterParser(file) : new SimpleParameterParser(file);
        pars = parser.parse();
    }

    private boolean isRndnet(String name) {
        return ParameterParser.RNDNET_NAME.equals(name);
    }

    public String getPar(String key) {
        return pars.getOrDefault(key, null);
    }

    public double getDoublePar(String key) {
        return Double.parseDouble(getPar(key));
    }

    public int getIntPar(String key) {
        return Integer.parseInt(getPar(key));
    }

    public boolean getBoolPar(String key) {
        return Boolean.parseBoolean(getPar(key));
    }

    public String[] getArrPar(String key) {
        return hasPar(key) ? parseArr(getPar(key)) : null;
    }

    public double[] getDoubleArrPar(String key) {
        return hasPar(key) ? parseDoubleArr(getPar(key)) : null;
    }

    public Double[] getObjDoubleArrPar(String key) {
        return hasPar(key) ? parseObjDoubleArr(getPar(key)) : null;
    }

    public int[] getIntArrPar(String key) {
        return hasPar(key) ? parseIntArr(getPar(key)) : null;
    }

    public boolean[] getBoolArrPar(String key) {
        return hasPar(key) ? parseBoolArr(getPar(key)) : null;
    }

    public boolean hasPar(String key) {
        return getPar(key) != null;
    }

    public static void main(String[] args) {
        final ParameterFactory parser = new ParameterFactory(args[0]);
        System.out.println(parser.pars);
    }

    private static abstract class ParameterParser {
        static final String RNDNET_NAME = ".prm";
        String file;

        ParameterParser(String file) {
            this.file = file;
        }

        public abstract Map<String, String> parse();
    }

    /**
     * Parse json parameters (RnDnet)
     */
    private static class RndnetParameterParser extends ParameterParser {

        RndnetParameterParser(String file) {
            super(file);
        }

        @Override
        public Map<String, String> parse() {
            Map<String, String> pars = new HashMap<>();
            try {
                Gson gson = new Gson();
                Type listType = new TypeToken<List<ParItem>>() {
                }.getType();
                final String sJson = loadFile(file);
                final List<ParItem> list = gson.fromJson(sJson, listType);
                for (ParItem item : list) {
                    pars.put(item.key, item.val);
                }
//                System.out.println(gson.toJson(list));
            } catch (IOException e) {
                e.printStackTrace();
            }
            return pars;
        }

        private static class ParItem {
            private String key;
            private String val;
            private String group;
            private String description;
            private ParConfig config;
        }

        private static class ParConfig {
            private String widget;
            private String get;
            private String set;
            private List<ParSetup> setup;
        }

        private static class ParSetup {
            private String method;
            private String args;
            private String kwargs;
        }

        /*
          {
            "key": "category",
            "val": "\"cat\"",
            "group": "",
            "description": "",
            "config": {
              "widget": "QComboBox",
              "get": "currentText",
              "set": "setCurrentText",
              "setup": [
                {
                  "method": "addItems",
                  "args": "[[\"cat\", \"dog\"]]",
                  "kwargs": "{}"
                }
              ],
              "items": []
            }
          }
        */
    }

    /**
     * Parse txt parameters (key=value)
     */
    private static class SimpleParameterParser extends ParameterParser {
        public SimpleParameterParser(String file) {
            super(file);
        }

        @Override
        public Map<String, String> parse() {
            Map<String, String> pars = new HashMap<>();
            try {
                Properties props = new Properties();
                try (FileReader fr = new FileReader(file)) {
                    props.load(fr);
                }
                for (Object key : props.keySet()) {
                    pars.put((String) key, props.getProperty((String) key));
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            return pars;
        }
    }

}
