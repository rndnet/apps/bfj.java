package com.rndnet.bfj;

import org.apache.commons.io.IOUtils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.reflect.Array;
import java.nio.charset.StandardCharsets;

public class Utils {

    // File utils
    public static String loadFile(String file) throws IOException {
        return loadFile(new File(file));
    }

    public static String loadFile(File file) throws IOException {
        try (FileInputStream is = new FileInputStream(file)) {
            return IOUtils.toString(is, StandardCharsets.UTF_8.name());
        }
    }

    public static void saveToFile(File file, String val) throws IOException {
        try (FileOutputStream os = new FileOutputStream(file)) {
            IOUtils.write(val, os, StandardCharsets.UTF_8.name());
        }
    }

    // Parse utils
    public static String[] parseArr(String val) {
        if (val.startsWith("\"") && val.endsWith("\"")) {
            val = val.substring(1, val.length() - 1);
        }
        if (val.startsWith("[") && val.endsWith("]")) {
            val = val.substring(1, val.length() - 1);
        }
        if (val.length() > 0) {
            final String[] arr = val.split(",");
            for (int i = 0; i < arr.length; i++) {
                arr[i] = arr[i].trim();
                if (arr[i].startsWith("\"") && arr[i].endsWith("\"")) {
                    arr[i] = arr[i].substring(1, arr[i].length() - 1);
                }
            }
            return arr;
        } else {
            return new String[0];
        }
    }

    public static double[] parseDoubleArr(String val) {
        final String[] sArr = parseArr(val);
        double[] arr = new double[sArr.length];
        for (int i = 0; i < arr.length; i++) {
            arr[i] = Double.parseDouble(sArr[i]);
        }
        return arr;
    }

    public static Double[] parseObjDoubleArr(String val) {
        final String[] sArr = parseArr(val);
        Double[] arr = new Double[sArr.length];
        for (int i = 0; i < arr.length; i++) {
            if (sArr[i].length() > 0) {
                arr[i] = Double.parseDouble(sArr[i]);
            } else {
                arr[i] = null;
            }
        }
        return arr;
    }

    public static int[] parseIntArr(String val) {
        final String[] sArr = parseArr(val);
        int[] arr = new int[sArr.length];
        for (int i = 0; i < arr.length; i++) {
            arr[i] = Integer.parseInt(sArr[i]);
        }
        return arr;
    }

    public static boolean[] parseBoolArr(String val) {
        final String[] sArr = parseArr(val);
        boolean[] arr = new boolean[sArr.length];
        for (int i = 0; i < arr.length; i++) {
            if ("true".equalsIgnoreCase(sArr[i]) || "false".equalsIgnoreCase(sArr[i])) {
                arr[i] = Boolean.parseBoolean(sArr[i]);
            } else {
                throw new IllegalArgumentException("Wrong boolean value: " + sArr[i]);
            }
        }
        return arr;
    }

    public static String objToString(Object obj) {
        if (obj == null) {
            return null;
        }
        if (obj instanceof Integer || obj instanceof Double || obj instanceof Float || obj instanceof Boolean) {
            return String.valueOf(obj);
        } else if (obj instanceof String) {
            return "\"" + obj + "\"";
        } else if (obj.getClass().isArray()) {
            final int len = Array.getLength(obj);
            if (len == 0) {
                return "[]";
            } else {
                if (Array.get(obj, 0).getClass().isArray()) {
                    throw new IllegalArgumentException("Not implemented for 2-dim arrays");
                }
                StringBuilder sb = new StringBuilder();
                sb.append('[');
                for (int i = 0; i < len; i++) {
                    if (i > 0) sb.append(", ");
                    sb.append(objToString(Array.get(obj, i)));
                }
                sb.append(']');
                return sb.toString();
            }
        } else {
            return null;
        }
    }

    // Data utils
    public static double[][] transpose(double[][] ar) {
        double[][] tmp = new double[ar[0].length][ar.length];
        for (int i = 0; i < ar[0].length; i++) {
            for (int j = 0; j < ar.length; j++) {
                tmp[i][j] = ar[j][i];
            }
        }
        return tmp;
    }

    public static float[][] transposeToFloat(double[][] ar) {
        float[][] tmp = new float[ar[0].length][ar.length];
        for (int i = 0; i < ar[0].length; i++) {
            for (int j = 0; j < ar.length; j++) {
                tmp[i][j] = (float) ar[j][i];
            }
        }
        return tmp;
    }

    public static float[][] transpose(float[][] ar) {
        float[][] tmp = new float[ar[0].length][ar.length];
        for (int i = 0; i < ar[0].length; i++) {
            for (int j = 0; j < ar.length; j++) {
                tmp[i][j] = ar[j][i];
            }
        }
        return tmp;
    }

    public static double[][] transposeToDouble(float[][] ar) {
        double[][] tmp = new double[ar[0].length][ar.length];
        for (int i = 0; i < ar[0].length; i++) {
            for (int j = 0; j < ar.length; j++) {
                tmp[i][j] = ar[j][i];
            }
        }
        return tmp;
    }

}
