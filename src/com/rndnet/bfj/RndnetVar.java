package com.rndnet.bfj;

import ch.systemsx.cisd.hdf5.HDF5DataSetInformation;
import ch.systemsx.cisd.hdf5.IHDF5Reader;
import ch.systemsx.cisd.hdf5.IHDF5Writer;
import org.apache.commons.io.FilenameUtils;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.util.Date;

import static com.rndnet.bfj.Utils.*;

public class RndnetVar {

    enum Type {
        INT, DOUBLE, BOOLEAN, STRING, UNKNOWN
    }

    private RndnetPackage pkg;
    private String fileName;
    private String name;
    private Type type;
    private boolean doublePrec;
    private boolean arrFlag;
    private int dim;
    private boolean fileFlag;
    private boolean h5Flag;

    private int intVal;
    private double doubleVal;
    private boolean boolVal;
    private String stringVal;
    private Object objectVal;

    // Variable of input package
    public RndnetVar(RndnetPackage pkg, String fileName, String name) {
        this(pkg, fileName, name, null, false, -1);
    }

    // Variable of input package
    public RndnetVar(RndnetPackage pkg, String fileName, String name, Type type, boolean arrFlag, int dim) {
        this.pkg = pkg;
        this.fileName = fileName;
        this.name = name;
        this.type = type;
        this.arrFlag = arrFlag;
        this.dim = dim;
        fileFlag = FilenameUtils.getName(fileName).contains(".");  // has ext
        h5Flag = "h5".equals(FilenameUtils.getExtension(fileName));  // h5 ext
    }

    // Variable of output package
    public RndnetVar(RndnetPackage pkg, String name, boolean h5Flag) {
        this(pkg, name, h5Flag, null, false);
    }

    // Variable of output package
    public RndnetVar(RndnetPackage pkg, String name, boolean h5Flag, Type type, boolean doublePrec) {
        this.pkg = pkg;
        this.name = name;
        this.h5Flag = h5Flag;
        this.type = type;
        this.doublePrec = doublePrec;
        fileFlag = h5Flag;
        fileName = h5Flag ? name + ".h5" : name;
    }

    public void setPkg(RndnetPackage pkg) {
        this.pkg = pkg;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getName() {
        return name;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public boolean isDoublePrec() {
        return doublePrec;
    }

    public boolean isArrFlag() {
        return arrFlag;
    }

    public int getDim() {
        return dim;
    }

    public boolean isFileFlag() {
        return fileFlag;
    }

    public boolean isH5Flag() {
        return h5Flag;
    }

    // Get values
    public int getIntVal() {
        return fileFlag ? (Integer) objectVal : intVal;
    }

    public double getDoubleVal() {
        return fileFlag ? (Double) objectVal : doubleVal;
    }

    public float getFloatVal() {
        return fileFlag ? (Float) objectVal : (float) doubleVal;
    }

    public boolean getBoolVal() {
        return fileFlag ? (Boolean) objectVal : boolVal;
    }

    public String getStringVal() {
        return fileFlag ? (String) objectVal : stringVal;
    }

    public Date getDateVal() {
        try {
            return DateFormats.parseDateZero(DateFormats.PKG_DATE_FORMAT, fileFlag ? (String) objectVal : stringVal);
        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }
    }

    public int[] getIntArrVal() {
        return (int[]) objectVal;
    }

    public double[] getDoubleArrVal() {
        return (double[]) objectVal;
    }

    public float[] getFloatArrVal() {
        return (float[]) objectVal;
    }

    public boolean[] getBoolArrVal() {
        return (boolean[]) objectVal;
    }

    public String[] getStringArrVal() {
        return (String[]) objectVal;
    }

    public int[][] getIntMatrixVal() {
        return (int[][]) objectVal;
    }

    public double[][] getDoubleMatrixVal() {
        return (double[][]) objectVal;
    }

    public float[][] getFloatMatrixVal() {
        return (float[][]) objectVal;
    }

    public boolean[][] getBoolMatrixVal() {
        return (boolean[][]) objectVal;
    }

    public String[][] getStringMatrixVal() {
        return (String[][]) objectVal;
    }

    public Object getObjectVal() {
        return objectVal;
    }

    public Object getVal() {
        if (objectVal != null) {
            return objectVal;
        } else if (type == Type.INT) {
            return intVal;
        } else if (type == Type.DOUBLE) {
            return doubleVal;
        } else if (type == Type.BOOLEAN) {
            return boolVal;
        } else if (type == Type.STRING) {
            return stringVal;
        } else {
            return null;
        }
    }

    /**
     * Transpose matrix
     * @return - matrix
     */
    public double[][] getTransposedDoubleVal() {
        if (type != Type.DOUBLE || !arrFlag || dim != 2) {
            return null;
        }
        if (doublePrec) {
            return Utils.transpose(getDoubleMatrixVal());
        } else {
            return Utils.transposeToDouble(getFloatMatrixVal());
        }
    }

    /**
     * Load variable
     * @throws IOException -
     */
    public void load() throws IOException {
        if (!fileFlag) {  // meta
            String val = loadFile(new File(pkg.getMetaPath(), fileName));
            parseMeta(val);
        } else if (h5Flag) {
            loadH5();
        } else {
            type = Type.UNKNOWN;
        }
    }

    /**
     * Parse string value of meta variable, recognise it's type
     * @param val - string value
     */
    public void parseMeta(String val) {
        if (val.startsWith("\"") && val.endsWith("\"")) {
            val = val.substring(1, val.length() - 1);
        }
        stringVal = val;
        if (type != null) {
            parseTypedVal(val, type);
        } else {
            arrFlag = val.startsWith("[") && val.endsWith("]");
            try {
                parseTypedVal(val, Type.INT);
                type = Type.INT;
            } catch (Exception e) {
                try {
                    parseTypedVal(val, Type.DOUBLE);
                    type = Type.DOUBLE;
                } catch (Exception e2) {
                    try {
                        parseTypedVal(val, Type.BOOLEAN);
                        type = Type.BOOLEAN;
                    } catch (Exception e3) {
                        parseTypedVal(val, Type.STRING);
                        type = Type.STRING;
                    }
                }
            }
        }
    }

    /**
     * Parse string value of meta variable with known type
     * @param val - string value
     */
    private void parseTypedVal(String val, Type valType) {
        if (valType == Type.INT) {
            if (!arrFlag) {
                intVal = Integer.parseInt(val);
            } else {
                objectVal = parseIntArr(val);
            }
        } else if (valType == Type.DOUBLE) {
            if (!arrFlag) {
                doubleVal = Double.parseDouble(val);
            } else {
                objectVal = parseDoubleArr(val);
            }
        } else if (valType == Type.BOOLEAN) {
            if (!arrFlag) {
                if ("true".equalsIgnoreCase(val) || "false".equalsIgnoreCase(val)) {
                    boolVal = "true".equalsIgnoreCase(val);
                } else {
                    throw new IllegalArgumentException("Wrong boolean value: " + val);
                }
            } else {
                objectVal = parseBoolArr(val);
            }
        } else if (valType == Type.STRING) {
            if (!arrFlag) {
                stringVal = val;
            } else {
                objectVal = parseArr(val);
            }
        }
    }

    /**
     * Read object from h5 file
     */
    private void loadH5() {
        final IHDF5Reader reader = Hdf5IOFactory.createH5Reader(new File(pkg.getPath(), fileName));
        setH5Info(reader.getDataSetInformation(name));
        objectVal = Hdf5IOFactory.readH5Value(reader, name);
    }

    private void setH5Info(HDF5DataSetInformation info) {
        final String dataType = info.getTypeInformation().getDataClass().name();
        arrFlag = !info.isScalar();
        dim = arrFlag ? info.getDimensions().length : 1;
        doublePrec = info.getTypeInformation().getElementSize() > 4;
        final boolean timeStamp = info.isTimeStamp();  // todo
        type = Hdf5IOFactory.H5DT_INTEGER.equals(dataType) ? Type.INT :
                Hdf5IOFactory.H5DT_FLOAT.equals(dataType) ? Type.DOUBLE :
                        Hdf5IOFactory.H5DT_BOOLEAN.equals(dataType) ? Type.BOOLEAN :
                                Hdf5IOFactory.H5DT_STRING.equals(dataType) ? Type.STRING : Type.UNKNOWN;
    }

    /**
     * Save variable value to meta or h5 file
     * @throws IOException -
     */
    public void save() throws IOException {
        saveVal(getVal());
    }

    /**
     * Save object to meta or h5 file
     * @throws IOException -
     */
    public void saveVal(Object val) throws IOException {
        objectVal = val;
        if (h5Flag) {
            final IHDF5Writer writer = Hdf5IOFactory.createH5Writer(new File(pkg.getPath(), fileName));
            if (val instanceof Float) {
                if (doublePrec) {
                    Hdf5IOFactory.writeH5Double(writer, name, ((Float) val).doubleValue());
                } else {
                    Hdf5IOFactory.writeH5Float(writer, name, (Float) val);
                }
            } else if (val instanceof Double) {
                if (type == null || doublePrec) {
                    Hdf5IOFactory.writeH5Double(writer, name, (Double) val);
                } else {
                    Hdf5IOFactory.writeH5Float(writer, name, ((Double) val).floatValue());
                }
            } else {
                Hdf5IOFactory.writeH5Value(writer, name, val);
            }
        } else if (!fileFlag) {
            saveToFile(new File(pkg.getMetaPath(), fileName), objToString(val));
        }
    }

    /**
     * Transpose matrix and save it to h5 file
     * @param val - matrix
     */
    public void saveTransposedDoubleVal(double[][] val) {
        if (!h5Flag) {
            throw new IllegalArgumentException("Double array can be saved into h5 file only");
        }
        final IHDF5Writer writer = Hdf5IOFactory.createH5Writer(new File(pkg.getPath(), fileName));
        if (type != null && !doublePrec) {
            final float[][] matrix = transposeToFloat(val);
            Hdf5IOFactory.writeH5FloatMatrix(writer, name, matrix);
        } else {
            final double[][] matrix = transpose(val);
            Hdf5IOFactory.writeH5DoubleMatrix(writer, name, matrix);
        }
    }

    public String toString() {
        return "RndnetVar{" +
                "name=" + name +
                ", fileName=" + fileName +
                ", type=" + (type != null ? type.name() : null) +
                ", arrFlag=" + arrFlag +
                ", dim=" + dim +
                ", doublePrec=" + doublePrec +
                ", strVal=" + getStringVal() +
                ", value=" + (h5Flag ? "<h5>" : objToString(getVal())) +
                '}';
    }

    @Override
    public RndnetVar clone() {
        final RndnetVar clone = new RndnetVar(null, fileName, name, type, arrFlag, dim);
        clone.doublePrec = doublePrec;
        clone.intVal = intVal;
        clone.doubleVal = doubleVal;
        clone.boolVal = boolVal;
        clone.stringVal = stringVal;
        clone.objectVal = objectVal;
        return clone;
    }

}
